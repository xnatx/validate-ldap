# Validate LDAP #

This tool provides a way to validate a connection to an LDAP server.

## Specifying LDAP properties ##

You can test the connection by specifying values in a properties file. By
default, the script looks for the properties file **ldap.properties** unless
another file is specified on the command line. If the script can't find
**ldap.properties** and no other properties file is specified, it falls back
onto a set of default property values:

| Property      | Default value             | Description                       |
| ------------- | ------------------------- | --------------------------------- |
| address       | ldap://ldap.xnat.org      | Address of the LDAP server        |
| userdn        | cn=admin,dc=xnat,dc=org   | DN for the look-up user           |
| password      | password                  | Password for the look-up users    |
| search.base   | ou=users,dc=xnat,dc=org   | Search base for user look-ups     |
| search.filter | (uid={0})                 | Filter criteria for user look-ups |
| user          | asmith                    | Username to validate              |
| pass          | password                  | Password for user validation      |

These default values are the same as specified in the provided
[ldap-provider.properties](ldap-provider.properties). They are configured to
work with the default configuration of the [XNAT LDAP Vagrant project](https://bitbucket.org/xnatdev/xnat-ldap-vagrant), with the
caveat that you'll need to map the server address **ldap.xnat.org** to the IP
address (by default 10.1.1.22) configured for the resulting virtual machine.

Note that, if you specify a properties file and omit any of these properties,
the script will fall back to using the default value for the missing properties.

## Running LDAP validation ##

To run the validation script, you need to have [Groovy](https://groovy-lang.org)
installed. The script dynamically downloads a number of dependencies, so an
active internet connection is also required to run the script as configured (see
below for information on running the script without an internet connection, e.g.
in secure firewalled environments).

Once you have the prerequisites to run the script, you can run the validation
with the default like this:

```bash
$ groovy src/ValidateLdap.groovy
```

If you want to specify property values other than the defaults, just add the
name of the properties file you want to use (which can include the path to the
properties file):

```bash
$ groovy src/ValidateLdap.groovy ../configurations/ldap-1.properties
```

## Running script without internet connection ##

The validation script will download its dependencies automatically if you have
an active internet connection, but you can still run it if you need to work in
an environment where you can't connect to the internet. First, you need to
download the required dependencies from an environment where you _can_ access
the internet. The required dependencies are:

* [Spring Security Core 4.2.4.RELEASE](http://search.maven.org/remotecontent?filepath=org/springframework/security/spring-security-core/4.2.4.RELEASE/spring-security-core-4.2.4.RELEASE.jar)
* [Spring Security LDAP 4.2.4.RELEASE](http://search.maven.org/remotecontent?filepath=org/springframework/security/spring-security-ldap/4.2.4.RELEASE/spring-security-ldap-4.2.4.RELEASE.jar)
* [Spring LDAP Core 2.3.2.RELEASE](http://search.maven.org/remotecontent?filepath=org/springframework/ldap/spring-ldap-core/2.3.2.RELEASE/spring-ldap-core-2.3.2.RELEASE.jar)
* [Spring AOP 4.3.12.RELEASE](http://search.maven.org/remotecontent?filepath=org/springframework/spring-aop/4.3.12.RELEASE/spring-aop-4.3.12.RELEASE.jar)
* [Spring Beans 4.3.12.RELEASE](http://search.maven.org/remotecontent?filepath=org/springframework/spring-beans/4.3.12.RELEASE/spring-beans-4.3.12.RELEASE.jar)
* [Spring Core 4.3.12.RELEASE](http://search.maven.org/remotecontent?filepath=org/springframework/spring-core/4.3.12.RELEASE/spring-core-4.3.12.RELEASE.jar)
* [Spring Context 4.3.12.RELEASE](http://search.maven.org/remotecontent?filepath=org/springframework/spring-context/4.3.12.RELEASE/spring-context-4.3.12.RELEASE.jar)
* [Spring Expression 4.3.12.RELEASE](http://search.maven.org/remotecontent?filepath=org/springframework/spring-expression/4.3.12.RELEASE/spring-expression-4.3.12.RELEASE.jar)
* [Spring Tx 4.3.12.RELEASE](http://search.maven.org/remotecontent?filepath=org/springframework/spring-tx/4.3.12.RELEASE/spring-tx-4.3.12.RELEASE.jar)
* [Slf4j API 1.7.25](http://search.maven.org/remotecontent?filepath=org/slf4j/slf4j-api/1.7.25/slf4j-api-1.7.25.jar)
* [Slf4j No-Op 1.7.25](http://search.maven.org/remotecontent?filepath=org/slf4j/slf4j-nop/1.7.25/slf4j-nop-1.7.25.jar)
* [AOP Alliance 1.0](http://search.maven.org/remotecontent?filepath=aopalliance/aopalliance/1.0/aopalliance-1.0.jar)
* [Commons Logging 1.2](http://search.maven.org/remotecontent?filepath=commons-logging/commons-logging/1.2/commons-logging-1.2.jar)

A script named [download-dependencies.sh](download-dependencies.sh) is provided
to make this easier. It uses the common **curl** command to download
these files and places them in the **lib** folder. Once you've downloaded the dependencies, move them to the environment from which you want to validate your
LDAP connection and configuration.

Now you need to make one change to the [ValidateLdap.groovy](src/ValidateLdap.groovy) script before executing it. Delete or comment out the first line of the script,
which starts with **@Grapes**:

```groovy
// @Grapes([@Grab("org.springframework.security:spring-security-ldap:4.2.4.RELEASE"), @Grab("org.slf4j:slf4j-nop:1.7.25")])
```

Now invoke the script like this:

```bash
$ groovy -cp $(echo lib/*.jar | tr ' ' ':') src/ValidateLdap.groovy
```

You can also specify a properties file, the same as with the regular execution:

```bash
$ groovy -cp $(echo lib/*.jar | tr ' ' ':') src/ValidateLdap.groovy ldap-provider.properties
```
