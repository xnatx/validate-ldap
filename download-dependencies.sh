#!/bin/bash
#
# validate-ldap: download-dependencies.sh
# XNAT http://www.xnat.org
# Copyright (c) 2018, Washington University School of Medicine and Howard Hughes Medical Institute
# All Rights Reserved
#  
# Released under the Simplified BSD.
# 

mkdir -p lib
curl --output lib/spring-security-core-4.2.4.RELEASE.jar "http://search.maven.org/remotecontent?filepath=org/springframework/security/spring-security-core/4.2.4.RELEASE/spring-security-core-4.2.4.RELEASE.jar"
curl --output lib/spring-security-ldap-4.2.4.RELEASE.jar "http://search.maven.org/remotecontent?filepath=org/springframework/security/spring-security-ldap/4.2.4.RELEASE/spring-security-ldap-4.2.4.RELEASE.jar"
curl --output lib/spring-ldap-core-2.3.2.RELEASE.jar "http://search.maven.org/remotecontent?filepath=org/springframework/ldap/spring-ldap-core/2.3.2.RELEASE/spring-ldap-core-2.3.2.RELEASE.jar"
curl --output lib/spring-aop-4.3.12.RELEASE.jar "http://search.maven.org/remotecontent?filepath=org/springframework/spring-aop/4.3.12.RELEASE/spring-aop-4.3.12.RELEASE.jar"
curl --output lib/spring-beans-4.3.12.RELEASE.jar "http://search.maven.org/remotecontent?filepath=org/springframework/spring-beans/4.3.12.RELEASE/spring-beans-4.3.12.RELEASE.jar"
curl --output lib/spring-core-4.3.12.RELEASE.jar "http://search.maven.org/remotecontent?filepath=org/springframework/spring-core/4.3.12.RELEASE/spring-core-4.3.12.RELEASE.jar"
curl --output lib/spring-context-4.3.12.RELEASE.jar "http://search.maven.org/remotecontent?filepath=org/springframework/spring-context/4.3.12.RELEASE/spring-context-4.3.12.RELEASE.jar"
curl --output lib/spring-expression-4.3.12.RELEASE.jar "http://search.maven.org/remotecontent?filepath=org/springframework/spring-expression/4.3.12.RELEASE/spring-expression-4.3.12.RELEASE.jar"
curl --output lib/spring-tx-4.3.12.RELEASE.jar "http://search.maven.org/remotecontent?filepath=org/springframework/spring-tx/4.3.12.RELEASE/spring-tx-4.3.12.RELEASE.jar"
curl --output lib/slf4j-nop-1.7.25.jar "http://search.maven.org/remotecontent?filepath=org/slf4j/slf4j-nop/1.7.25/slf4j-nop-1.7.25.jar"
curl --output lib/slf4j-api-1.7.25.jar "http://search.maven.org/remotecontent?filepath=org/slf4j/slf4j-api/1.7.25/slf4j-api-1.7.25.jar"
curl --output lib/aopalliance-1.0.jar "http://search.maven.org/remotecontent?filepath=aopalliance/aopalliance/1.0/aopalliance-1.0.jar"
curl --output lib/commons-logging-1.2.jar "http://search.maven.org/remotecontent?filepath=commons-logging/commons-logging/1.2/commons-logging-1.2.jar"
