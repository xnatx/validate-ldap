/*
 * validate-ldap: TestUrls.groovy
 * XNAT http://www.xnat.org
 * Copyright (c) 2018, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *  
 * Released under the Simplified BSD.
 */

@Grapes(@Grab("org.codehaus.groovy.modules.http-builder:http-builder:0.7.1"))

import groovyx.net.http.HTTPBuilder

import static groovyx.net.http.ContentType.TEXT
import static groovyx.net.http.Method.GET

def workingUrls = []
["http://www.dcm4che.org"     : ["/maven2/org/nrg/parent/1.7.4/parent-1.7.4.pom"],
 "https://jcenter.bintray.com": ["/org/nrg/parent/1.7.4/parent-1.7.4.pom"],
 "https://nrgxnat.jfrog.io"   : ["/nrgxnat/ext-release/org/nrg/parent/1.7.4/parent-1.7.4.pom", "/nrgxnat/libs-release/org/nrg/parent/1.7.4/parent-1.7.4.pom", "/nrgxnat/libs-snapshot/org/nrg/parent/1.7.4/parent-1.7.4.pom"],
 "https://repo1.maven.org"    : ["/maven2/org/nrg/parent/1.7.4/parent-1.7.4.pom"]].each { url, paths ->
    def http = new HTTPBuilder()
    paths.each { path ->
        http.request(url, GET, TEXT) { request ->
            uri.path = path
            headers."User-Agent" = "Mozilla/5.0 Firefox/3.0.4"
            headers.Accept = "*/*"

            response.success = { response, reader ->
                workingUrls << "${url}/${path}"
                println "Found requested artifact at URL ${url}/${path}"
                println "Got response: ${response.statusLine}"
                println "Content-Type: ${response.headers."Content-Type"}"
                // println reader.text
            }
            response."401" = { response ->
                println "Not authorized to view the requested artifact at URL ${url}/${path}"
            }
            response."403" = { response ->
                println "Must be authenticated to view the requested artifact at URL ${url}/${path}"
            }
            response."404" = { response ->
                println "Did not find the requested artifact at URL ${url}/${path}"
            }
            response."500" = { response ->
                println "Encountered a server error trying to retrieve the requested artifact at URL ${url}/${path}"
            }
            response.failure = { response ->
                println "Got an unknown failure response ${response.status} trying to retrieve the requested artifact at URL ${url}/${path}"
                println "Message: ${response.statusLine.reasonPhrase}"
            }
        }
    }
}
println workingUrls.isEmpty() ? "Didn't find the requested URL at any of the specified locations" : "Successfully retrieved the artifact from the following URL(s): ${workingUrls}"
